//----------------------------------------------------
// Sun Shafts
//----------------------------------------------------
#define SS_FARPLANE 180
#define SS_DENSITY 0.7
#define SS_BLEND_FACTOR 0.9
#define SS_QUALITY 5