#ifndef	_SUN_SHAFTS
	#define _SUN_SHAFTS
	#include "common.h"
	#include "_shaders_config.h"
	
	#ifndef SS_QUALITY
		#define SS_SAMPLES 140
	#elif SS_QUALITY == 5
		#define SS_SAMPLES 140
	#elif SS_QUALITY == 4
		#define SS_SAMPLES 110
	#elif SS_QUALITY == 3
		#define SS_SAMPLES 90
	#elif SS_QUALITY == 2
		#define SS_SAMPLES 70
	#elif SS_QUALITY == 1
		#define SS_SAMPLES 50
	#endif
	
	float4 convert_to_screen_space(float4 proj)
	{
		float4 screen;
		screen.x = (proj.x + proj.w)*0.5;
		screen.y = (proj.w - proj.y)*0.5;
		screen.z = proj.z;
		screen.w = proj.w;
		return screen;
	} 

	float normalize_depth(float depth) {
		return (saturate(depth/SS_FARPLANE));
	}
	
	float3 blend_soft_light(float3 a, float3 b) {
		float3 c = 2 * a * b + a * a * (1 - 2 * b);
		float3 d = sqrt(a) * (2 * b - 1) + 2 * a * (1 - b);
		return ( b < 0.5 )? c : d;
	}

	float3 godrays(float2 tc, float3 init_color) {  
		#ifdef USE_SUN_SHAFTS
			float3 out_color = float3(0,0,0);
			float sun_dist = SS_FARPLANE / (sqrt(1 - L_sun_dir_w.y * L_sun_dir_w.y));
			float4 sun_pos_projected = mul( m_VP, float4( sun_dist * L_sun_dir_w + eye_position, 1 ) );
			float4 sun_pos_screen = convert_to_screen_space(sun_pos_projected)/sun_pos_projected.w;
			float2 sun_vec_screen = sun_pos_screen.xy - tc;
			
			float angle_cos = dot(-eye_direction, normalize(L_sun_dir_w));
			float ray_fade = saturate(saturate(angle_cos)*(1 - saturate(dot(float2(sun_vec_screen.x/0.3, sun_vec_screen.y/0.45), float2(sun_vec_screen.x/0.3, sun_vec_screen.y/0.45))))*saturate(length(sun_vec_screen)*1000));
			float ray_light_step = 0.095;
			int ray_light_max = ray_light_step * (SS_SAMPLES*0.85);

			float mask = (saturate(angle_cos*0.1 + 0.9) + normalize_depth(tex2D(s_position, tc).z) - 0.99999) * SS_DENSITY;
			sun_vec_screen *= angle_cos * SS_DENSITY / SS_SAMPLES;
			float depth_accum = 0;
		
			for (float i = 0; i < SS_SAMPLES; i++) { 
				tc += sun_vec_screen;
				float blur_delta = 2 * (.5f/1024.f);	
				out_color += (tex2Dlod(s_image, float4(tc + blur_delta,0,0)) + tex2Dlod(s_image, float4(tc - blur_delta,0,0))) * (1.f/2.f) * (ray_light_max - (ray_light_step * i)) * SS_BLEND_FACTOR;
				depth_accum += saturate(1 - tex2D(s_position, tc).z*1000);
			}			
			out_color *= saturate(depth_accum/SS_SAMPLES);
			out_color *= ray_fade / SS_SAMPLES;
			out_color = init_color + out_color * L_sun_color * (1.0 - init_color);
			out_color = blend_soft_light(out_color, L_sun_color * mask * 0.5 + 0.5) - init_color;
			return out_color;
		#else
			return 0;
		#endif
	}  
#endif