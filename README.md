S.T.A.L.K.E.R. Зов Припяти - ИНКУБАТОР.

• V.I.V.I.E.N.T. TEAM в ВК - https://vk.com/vivient_team_mods
• Материальная поддержка: 
		  Donation Allerts - https://www.donationalerts.com/r/vivient_team
		              QIWI - https://qiwi.com/n/KILN4TUR3
		          YooMoney - https://yoomoney.ru/fundraise/yIlXuwQxAvA.230322

Оригинальная игра не требуется для запуска!
• Запускать модификацию через "xrLauncher.exe" или через батник "xrLauncher.bat"
• Для слабых пк можете активировать опцию облегченных партиклов в менеджере модов "JSGME.exe"
• При первом запуске выберите и примените пресет качества графики, настройте управление как вам удобно.

Создатели мода:
• 4ECTOR - Сюжет и его реализация, озвучка
• MayLay - Программирование и графика

Актёры озвучки:
• Олег Коренев (HelgardRay) - главный герой (Крамер)
• Олег Кузьма (слесарь 6 разряда) - Ремарк
• Полина (PollyVader) - Морриган
• Артём К. (LucioTraum) - Хромой / Раненый сталкер
• Наталья Алейникова - Лелиана
• George_Marshall - Брутальные наёмники
• Никита (NikStrelok) - Профессор Соколов
• SeeFuck - Учёный Сербин, учёные-наёмники, Тесак
• Риддлер-Сталкер Энигма - Валентир
• Alien - Профессор Кириллов, Профессор Кипарис
• Влад Чекторов - Сталкер, бандит, ассистент
• SKETYFACE - музыкальное сопровождение,меню,амбиенты

Благодарность за участие, контент и помощь этим потрясающим людям:
• Greshnik13rs • Den-Stash • Артём Банников • Ninja_Nub • Angry Wolf • makar • aaz • Крим •
• Gefos • ABSOLUTE NATURE 4 • Emmis • Дизель • Azetrix • One_Shot • S.W.R.P • 3fallout3 • loner •
• DanteZ • Nestandart_5443 • makdm • Бармен • Phantom_86 • ferr-um • Shennondoah • 3vtiger •
• Demosfen • Viktor • Sin! • August Autumn • Егерь С Двустволкой • Salem • owlromeo •
• Over Media • lehnov1986 • Crazy Stalker • Open X-Ray • HollyWoodFX •
