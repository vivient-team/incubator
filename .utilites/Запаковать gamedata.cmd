@echo off
@chcp 1251>nul
@title date - %date% - time - %Time%
xcopy /y /o /e "..\gamedata" "..\..\incubator_packing\gamedata\"
@Echo Copy gamedata is Done!
xcopy /y /o /e "dbtool\*.*" "..\..\incubator_packing\"
@Echo Copy xrCompress is Done!
@cd ..\..\incubator_packing
@start xrCompress.exe gamedata -ltx datapack.ltx -pack -db -640 -nodelete -nocompress
@Echo Packing is Done!
pause